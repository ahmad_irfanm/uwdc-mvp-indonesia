<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $hidden  = [];

    // Trainings Relationships
    public function trainings() {
        return $this->belongsToMany(Training::class, 'training_tags');
    }
}
