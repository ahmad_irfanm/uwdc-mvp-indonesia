<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    function __construct(Tag $tag) {
        $this->tag = $tag;
    }

    public function index() {

        // get all tags
        $tags = $this->tag->orderBy('name', 'asc')->get();

        // response success
        return response()->json([
            'message' => 'Data loaded successful',
            'data'    => $tags,
        ]);

    }

    public function store(Request $request) {

        // validations
        $this->validate($request, [
            'name' => 'required',
        ]);

        // insert
        $tag = $this->tag->create([
            'name' => $request->name,
        ]);

        // response success
        return response()->json([
            'message' => 'Tag created successful',
            'data'    => $tag,
        ]);

    }
}
