<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    function __construct(Category $category) {
        $this->category = $category;
    }

    public function index() {

        // get all categories
        $categories = $this->category->orderBy('name', 'asc')->get();

        // response success
        return response()->json([
            'message' => 'Data loaded successful',
            'data'    => $categories,
        ]);

    }
}
