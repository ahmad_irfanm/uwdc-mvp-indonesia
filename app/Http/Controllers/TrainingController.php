<?php

namespace App\Http\Controllers;

use App\Models\Training;
use Illuminate\Http\Request;

class TrainingController extends Controller
{
    function __construct(Training $training) {
        $this->train = $training;
    }

    public function index() {

        // define user
        $user = request()->user();

        // get all trainings by user
        $trainings = $user->trainings()->orderBy('date', 'asc')->with(['category', 'type', 'tags'])->get();

        // response success
        return response()->json([
            'message' => 'Data loaded successful',
            'data'    => $trainings,
        ]);

    }

    public function store(Request $request) {

        // define user
        $user = request()->user();

        // validations
        $this->validate($request, [
            'date'    => 'required',
            'notes'   => 'required',
            'type_id' => 'required|numeric',
            'category_id' => 'required|numeric',
            'spent_time'  => 'required|numeric',
        ]);

        // insert
        $train = $user->trainings()->create([
            'date'    => $request->date,
            'notes'   => $request->notes,
            'type_id' => $request->type_id,
            'category_id' => $request->category_id,
            'spent_time'  => $request->spent_time * 60,
        ]);

        // insert tags
        if ($request->tags) {
            foreach ($request->tags as $tag) {
                $train->tags()->attach($tag);
            }
        }

        // response success
        return response()->json([
            'message' => 'Traning session created successful',
            'data'    => $train,
        ]);

    }

    public function show($id) {

        // define user
        $user = request()->user();

        // define training
        $training = $this->train->where('id', $id)->with(['tags'])->first();

        // handle forbidden training
        if (!$user->trainings()->where('id', $training->id)->first()) {
            return response()->json([
                'message' => 'Forbidden to access training',
            ], 422);
        }

        // response success
        return response()->json([
            'message' => 'Data loaded successful',
            'data'    => $training,
        ]);

    }

    public function update(Request $request, $id) {

        // define user
        $user = request()->user();

        // define training
        $training = $this->train->where('id', $id)->with(['tags'])->first();

        // handle forbidden training
        if (!$user->trainings()->where('id', $training->id)->first()) {
            return response()->json([
                'message' => 'Forbidden to access training',
            ], 422);
        }

        // validations
        $this->validate($request, [
            'date'    => 'required',
            'notes'   => 'required',
            'type_id' => 'required|numeric',
            'category_id' => 'required|numeric',
            'spent_time'  => 'required|numeric',
        ]);

        // update
        $training->update([
            'date'    => $request->date,
            'notes'   => $request->notes,
            'type_id' => $request->type_id,
            'category_id' => $request->category_id,
            'spent_time'  => $request->spent_time * 60,
        ]);

        // insert tags
        if ($request->tags) {
//            foreach ($training->tags as $tag) {
//                $tag->delete();
//            }
            foreach ($request->tags as $tag) {
                $training->tags()->attach($tag);
            }
        }

        // response success
        return response()->json([
            'message' => 'Traning session updated successful',
            'data'    => $training,
        ]);

    }
}
