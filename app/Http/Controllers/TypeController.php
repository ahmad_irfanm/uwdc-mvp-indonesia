<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    function __construct(Type $type) {
        $this->type = $type;
    }

    public function index() {

        // get all tags
        $types = $this->type->orderBy('name', 'asc')->get();

        // response success
        return response()->json([
            'message' => 'Data loaded successful',
            'data'    => $types,
        ]);

    }

    public function store(Request $request) {

        // validations
        $this->validate($request, [
            'name' => 'required',
        ]);

        // insert
        $type = $this->type->create([
            'name' => $request->name,
        ]);

        // response success
        return response()->json([
            'message' => 'Type created successful',
            'data'    => $type,
        ]);

    }
}
