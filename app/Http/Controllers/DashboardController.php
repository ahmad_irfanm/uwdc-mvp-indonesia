<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Tag;
use App\Models\Training;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    function __construct(Training $train, Tag $tag, Category $category) {
        $this->train = $train;
        $this->tag = $tag;
        $this->category = $category;
    }

    public function sum_working_hours() {
        // define user
        $user = request()->user();

        // count working hours
        $working_hours = $user->trainings()->get()->reduce(function ($carry, $item) {
            return $carry + $item->spent_time;
        }, 4);

        $last_month   = $user->trainings()->whereMonth('date', date('m'))->whereYear('date', date('Y'))->get()->reduce(function ($carry, $item) {
            return $carry + $item->spent_time;
        }, 4);

        // response success
        return response()->json([
            'message' => 'Data loaded successful',
            'data'    => [
                'workingHours' => number_format($working_hours / 60, 1, ','),
                'lastMonth' => number_format($last_month / 60, 1, ','),
            ],
        ]);
    }

    public function working_per_months() {

        // define user
        $user = request()->user();

        $months = [];
        $hours  = [];
        for ($i = 0; $i < 12; $i++) {

            $months[$i] = date('M', strtotime(date('2021-' . ($i+1) . '-01')));
            $hours[$i]  = $user->trainings()->whereMonth('date', $i+1)->whereYear('date', date('Y'))->get()->reduce(function ($carry, $item) {
                return $carry + $item->spent_time;
            }, 0) / 60;

        }

        // response success
        return response()->json([
            'message' => 'Data loaded successful',
            'data'    => [
                'months' => $months,
                'workingHours'  => $hours,
            ],
        ]);

    }

    public function tags_count() {

        // define user
        $user = request()->user();

        $tags = $this->tag->with(['trainings' => function ($training) use ($user) {
            return $training->where('user_id', $user->id)->get();
        }])->get()->map(function($tag) {
            $tag->training_count = $tag->trainings->count();
            return $tag;
        })->sortByDesc([
            function ($a, $b) { return $a['training_count'] < $b['training_count'];},
        ])->values()->all();

        // response success
        return response()->json([
            'message' => 'Data loaded successful',
            'data'    => [
                'tags' => $tags,
            ],
        ]);

    }

    public function categories() {

        // define user
        $user = request()->user();

        $time['category'] = $this->category->orderBy('name', 'asc')->get();
        $time['value']    = $time['category']->map(function($category) use ($user) {
           $user_category = $user->trainings()->where('category_id', $category->id)->get()->reduce(function ($carry, $item) {
               return $carry + $item->spent_time;
           }, 0) / 60;
           $user_category = $user_category > 0 ? $user_category / 60 : 0;
           return $user_category;
        });
        $time['category'] = $time['category']->map(function($category) {
            return $category->name;
        });

        $occured['category'] = $this->category->orderBy('name', 'asc')->get();
        $occured['value']    = $occured['category']->map(function($category) use ($user) {
            $user_category = $user->trainings()->where('category_id', $category->id)->get()->count();
            return $user_category;
        });
        $occured['category'] = $occured['category']->map(function($category) {
            return $category->name;
        });

        // response success
        return response()->json([
            'message' => 'Data loaded successful',
            'data'    => [
                'time' => $time,
                'occured' => $occured,
            ],
        ]);

    }

}
