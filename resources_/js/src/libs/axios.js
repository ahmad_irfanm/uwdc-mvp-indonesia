import Vue from 'vue'

// axios
import axios from 'axios'

const axiosIns = axios.create({
  // You can add your headers here
  // ================================
  baseURL: 'http://localhost:8002/api',
  // timeout: 1000,
    headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
})

Vue.prototype.$http = axiosIns

export default axiosIns
