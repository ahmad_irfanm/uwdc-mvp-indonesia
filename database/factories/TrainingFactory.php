<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Type;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class TrainingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $types = Type::get()->map(function($type) {
            return $type->id;
        });

        $categories = Category::get()->map(function($category) {
            return $category->id;
        });

        $users = User::get()->map(function($user) {
            return $user->id;
        });

        return [
            //
            'date' => Carbon::now(),
            'user_id' => $this->faker->randomElement($users),
            'type_id' => $this->faker->randomElement($types),
            'category_id' => $this->faker->randomElement($categories),
            'spent_time' => 4 * 60, // 4 hours,
            'notes' => $this->faker->sentence,
        ];
    }
}
