<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TagFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->word,
            'variant' => $this->faker->randomElement(['light-primary', 'light-warning', 'light-info', 'light-danger']),
        ];
    }
}
