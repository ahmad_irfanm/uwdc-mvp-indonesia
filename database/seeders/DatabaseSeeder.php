<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Tag;
use App\Models\Training;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

         User::create([
             'name' => 'Competitor 1',
             'email' => 'competitor1@skill17.com',
             'email_verified_at' => now(),
             'password' => bcrypt('demopass1'), // password
             'remember_token' => Str::random(10),
         ]);

        User::create([
            'name' => 'Competitor 2',
            'email' => 'competitor2@skill17.com',
            'email_verified_at' => now(),
            'password' => bcrypt('demopass2'), // password
            'remember_token' => Str::random(10),
        ]);

        \App\Models\Category::factory(5)->create();
        \App\Models\Type::factory(10)->create();
        \App\Models\Tag::factory(30)->create();
        \App\Models\Training::factory(50)->create();



        foreach (Training::all() as $train) {

            $tags = Tag::get()->map(function($tag) {
                return $tag->id;
            });

            $train->tags()->attach($faker->randomElement($tags));
        }
    }
}
