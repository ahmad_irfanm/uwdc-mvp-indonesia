<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [\App\Http\Controllers\AuthController::class, 'login']);
    Route::post('register', [\App\Http\Controllers\AuthController::class, 'register']);

    Route::group(['middleware' => 'auth:sanctum'], function() {
        Route::get('logout', [\App\Http\Controllers\AuthController::class, 'logout']);
        Route::get('user', [\App\Http\Controllers\AuthController::class, 'user']);
    });
});

Route::group(['middleware' => 'auth:sanctum'], function() {

    // Dashboard
    Route::group(['prefix' => 'dashboard'], function() {
        Route::get('sum-working-hours', [\App\Http\Controllers\DashboardController::class, 'sum_working_hours']);
        Route::get('workings-per-month', [\App\Http\Controllers\DashboardController::class, 'working_per_months']);
        Route::get('tags-count', [\App\Http\Controllers\DashboardController::class, 'tags_count']);
        Route::get('categories', [\App\Http\Controllers\DashboardController::class, 'categories']);
    });

    // Training Sessions
    Route::apiResource('training-sessions', \App\Http\Controllers\TrainingController::class);

    // Category
    Route::apiResource('categories', \App\Http\Controllers\CategoryController::class);

    // Types
    Route::apiResource('types', \App\Http\Controllers\TypeController::class);

    // Tags
    Route::apiResource('tags', \App\Http\Controllers\TagController::class);

});
